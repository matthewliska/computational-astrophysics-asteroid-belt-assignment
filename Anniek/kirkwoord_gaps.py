import numpy as np
from timeit import default_timer as timer
np.set_printoptions(threshold=np.nan)

"""Simulating Kirkwood gaps in the asteroid belt using symplectic Euler integrator"""


start = timer()


def acc_2(GM_jup, GM_sun, pos_ast, pos_jup, pos_sun, r3_ast_sun, r3_ast_jup):
    """Calculating the gravitational pull of the Sun and Jupiter on the asteroids."""
    acc = - GM_sun*(pos_ast - pos_sun)/r3_ast_sun - GM_jup *(pos_ast - pos_jup)/r3_ast_jup
    return acc


def acc_1(GM, pos_1, pos_2, r3_jup_sun):
    """Calculating the gravitational pull of body 1 on body 2."""
    acc = - GM*(pos_1 - pos_2)/r3_jup_sun
    return acc


def symplectic_integrator(N, h, nof_ast, nof_saves):
    """The symplectic integrator calculates the new velocities and positions of the asteroids
    and jupiter. Returning the final positions of the asteroids and Jupiter."""
    
    # Set up intermidate data-save moments
    save_list = list(np.linspace(1, N-1, nof_saves))

    # Constants
    a_jup = 5.2044
    e_jup = 0.0489
    GM_sun = 4*np.pi**2
    GM_jup = ((4*np.pi**2)/(1047.95))
    count = 0

    # Start values for the Sun and  jupiter
    pos_sun = np.array([[-0.00496]*nof_ast, [0]*nof_ast, [0]*nof_ast])
    pos_jup = np.array([[a_jup*(1 - e_jup)]*nof_ast, [0]*nof_ast, [0]*nof_ast])
    y_vel_jup = np.sqrt(GM_sun*(1 + e_jup)/(a_jup*(1 - e_jup)))
    vel_jup = np.array([[0] * nof_ast, [y_vel_jup] * nof_ast, [0] * nof_ast])
    vel_sun = np.array([[0] * nof_ast, [-y_vel_jup / 1049.27] * nof_ast, [0] * nof_ast])
    
    #Start values for the asteroids
    ast_vx, ast_vy = [], []
    a_list = np.random.uniform(2.0, 5.0, size=nof_ast)
    angle_list = np.random.uniform(0.0, 2.0*np.pi, size=nof_ast)
    e_list = np.random.uniform(0.0, 0.0, size=nof_ast)  # orbits become eccentric by itself
    z_list = np.random.uniform(-0.1, 0.1, size=nof_ast)  # Random small velocity
    v_list = np.random.uniform(0.95, 1.05, size=nof_ast)
    r = np.array(a_list*(1 - e_list))
    pos_ast = np.array([r*np.cos(angle_list), r*np.sin(angle_list), [0]*nof_ast])
    ast_vx = -np.sin(angle_list)*np.sqrt(GM_sun*(1 + e_list)/r)
    ast_vy = np.cos(angle_list)*np.sqrt(GM_sun*(1 + e_list)/r)
    vel_ast = np.array([v_list*ast_vx, v_list*ast_vy, z_list*np.sqrt(ast_vy**2 + ast_vx**2)])

    print 'number of years', N*h

    # Start the integration
    for n in range(N):

        # Distance between the asteroids and the Sun and Jupiter
        r3_ast_jup=((pos_ast[0] - pos_jup[0])*(pos_ast[0] - pos_jup[0]) + 
                    (pos_ast[1] - pos_jup[1])*(pos_ast[1] - pos_jup[1]) + 
                    (pos_ast[2] - pos_jup[2])*(pos_ast[2] - pos_jup[2]))**(3./2)
        r3_ast_sun=((pos_ast[0] - pos_sun[0])*(pos_ast[0] - pos_sun[0]) + 
                    (pos_ast[1] - pos_sun[1])*(pos_ast[1] - pos_sun[1]) + 
                    (pos_ast[2] - pos_sun[2])*(pos_ast[2] - pos_sun[2]))**(3./2)

        # Calculate the new positions for each asteriods
        acceleration_2 = acc_2(GM_jup, GM_sun, pos_ast, pos_jup, pos_sun, r3_ast_sun, r3_ast_jup)

        vel_ast = vel_ast + h * acceleration_2
        pos_ast = pos_ast + h * vel_ast

        # Delete asteroids that go out of the system
        if n%365==0 or n==N-1:
            for i in range(3):
                new_array = pos_ast[i,:]
                if i==0 or i==1:
                    condlist = [abs(new_array[:]) < 6.5]
                else:
                    condlist = [abs(new_array[:]) < 1.]
                choicelist = [new_array]
                result = np.select(condlist, choicelist)
                indices = np.nonzero(result)
                count += len(new_array) - len(indices[0])
                pos_ast = np.compress(result, pos_ast, axis=1)
                vel_ast = np.compress(result, vel_ast, axis=1)
                pos_jup = np.compress(result, pos_jup, axis=1)
                vel_jup = np.compress(result, vel_jup, axis=1)
                pos_sun = np.compress(result, pos_sun, axis=1)
                vel_sun = np.compress(result, vel_sun, axis=1)

        # Distance between the Sun and Jupiter
        r3_jup_sun = ((pos_jup[0] - pos_sun[0])*(pos_jup[0] - pos_sun[0]) + 
                      (pos_jup[1] - pos_sun[1])*(pos_jup[1] - pos_sun[1]) + 
                      (pos_jup[2] - pos_sun[2])*(pos_jup[2] - pos_sun[2]))**(3./2)

        # Calculate the new positions of Jupiter and the Sun    
        acc_jup = acc_1(GM_sun, pos_jup, pos_sun, r3_jup_sun) 
        acc_sun = acc_1(GM_jup, pos_sun, pos_jup, r3_jup_sun)

        vel_jup = vel_jup + h * acc_jup
        pos_jup = pos_jup + h * vel_jup
        vel_sun = vel_sun + h * acc_sun
        pos_sun = pos_sun + h * vel_sun

        # Write to text file 5 times per run
        if n==int(save_list[0]):
            print "Save at year", int(n*h)
            print "Number of asteroids", nof_ast-count
            np.save('data_ast' + str(int(n*h)) + '.npy', pos_ast)
            np.save('data_jup' + str(int(n*h)) + '.npy', pos_jup)
            np.save('data_sun' + str(int(n*h)) + '.npy', pos_sun)
            np.save('data_ast_vel' + str(int(n*h)) + '.npy', vel_ast)
            del save_list[0]

    jup_x = pos_jup[0]
    jup_y = pos_jup[1]
    jup_z = pos_jup[2]
    ast_x = pos_ast[0]
    ast_y = pos_ast[1]
    ast_z = pos_ast[2]
    ast_vx = vel_ast[0]
    ast_vy = vel_ast[1]
    ast_vz = vel_ast[2]

    return jup_x, jup_y, jup_z, ast_x, ast_y, ast_z, ast_vx, ast_vy, ast_vz


# Input parameters
h = 1./(2*365.)  # Timestep of 0.5 day
nof_cycles = int(100/h)  # Number of timesteps
nof_ast = 2500  # Number of asteroids
nof_saves = 5

# Call the function
sol = symplectic_integrator(nof_cycles, h, nof_ast, nof_saves)

# Keep track of calculation time needed
end = timer()
print "Time elapsed:", (end-start), "seconds"