import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.font_manager import FontProperties

'''Load data and plot histogram of semi-major axis, histogram of Jupiter Trojans 
distribution and a scatter plot of asteroid positions'''


# Adjust filename here
filename_end = '99'  
filename_begin = '0'

def loaddata(n):
    '''Loads data from files and returns arrays'''
    data_sun = np.load('data_sun' + str(n) + '.npy')
    data_jup = np.load('data_jup' + str(n) + '.npy')
    data_ast = np.load('data_ast' + str(n) + '.npy')
    data_ast_vel= np.load('data_ast_vel' + str(n) + '.npy')
    
    sun_x = data_sun[0]
    sun_y = data_sun[1]
    sun_z = data_sun[2]
        
    jup_x = data_jup[0]
    jup_y = data_jup[1]
    jup_z = data_jup[2]   
    
    ast_x = data_ast[0]
    ast_y = data_ast[1]
    ast_z = data_ast[2]
        
    vel_ast_x = data_ast_vel[0]
    vel_ast_y = data_ast_vel[1]
    vel_ast_z = data_ast_vel[2]
    
    return sun_x, sun_y, sun_z, jup_x, jup_y, jup_z, ast_x, ast_y, ast_z, vel_ast_x, vel_ast_y, vel_ast_z


sun_x_end, sun_y_end, sun_z_end, jup_x_end, jup_y_end, jup_z_end, ast_x_end, ast_y_end, ast_z_end, vel_ast_x_end, vel_ast_y_end, vel_ast_z_end = loaddata(filename_end)
sun_x_begin, sun_y_begin, sun_z_begin, jup_x_begin, jup_y_begin, jup_z_begin, ast_x_begin, ast_y_begin, ast_z_begin, vel_ast_x_begin, vel_ast_y_begin, vel_ast_z_begin = loaddata(filename_begin)


def distances(ast_x, ast_y, ast_z, vel_ast_x, vel_ast_y, vel_ast_z):
    '''Calculate radius, semi-major axis and orbital period of the asteroids'''
    mu = 4*np.pi**2
    
    a_list=[]
    p_list = []
    r_list=[]
    
    # Calculate semi-major axis, radius, and orbital period
    for n in range(len(ast_x)):
        r = np.sqrt(ast_x[n]**2+ast_y[n]**2+ast_z[n]**2)
        a = np.abs((mu*r/ (2*mu - (vel_ast_x[n]**2+vel_ast_y[n]**2+vel_ast_z[n]**2)*r)))
        p = (2*np.pi*np.sqrt(a**3/mu)) / 11.862  # Orbital period in Jupiter periods
        if a > 6.5:  # Remove asteroids with semi-major axis bigger than 6.5 AU
            continue 
        else:
            a_list.append(a)
            r_list.append(r)
        
        if p > 1.5:
            continue 
        else:
            p_list.append(p)
    
    return r_list, a_list, p_list


def trojans(jup_x, jup_y, jup_z, ast_x, ast_y, ast_z):
    '''Calculate the distribution of Jupiter Trojans'''
    trojans_list = []
    
    for i in range(len(ast_x)):
        
        if 5.05 < np.sqrt(ast_x[i]**2 + ast_y[i]**2) < 5.35:
            distance = np.sqrt((jup_x[i] - ast_x[i])**2 + (jup_y[i] - ast_y[i])**2)
            radius_jup = 5.2 
            angle_rad = np.arccos(1 - (distance**2 / (2*radius_jup**2)))
            angle_degree = angle_rad * (360./(2.*np.pi))
            trojans_list.append(angle_degree)
            
        else:
            continue 
        
    trojans_list = [x for x in trojans_list if str(x) != 'nan']  # Remove nan values
    
    return trojans_list


r_list_end, a_list_end, p_list_end = distances(ast_x_end, ast_y_end, ast_z_end, vel_ast_x_end, vel_ast_y_end, vel_ast_z_end)
r_list_begin, a_list_begin, p_list_begin = distances(ast_x_begin, ast_y_begin, ast_z_begin, vel_ast_x_begin, vel_ast_y_begin, vel_ast_z_begin)

trojans_list_end = trojans(jup_x_end, jup_y_end, jup_z_end, ast_x_end, ast_y_end, ast_z_end)
trojans_list_begin = trojans(jup_x_begin, jup_y_begin, jup_z_begin, ast_x_begin, ast_y_begin, ast_z_begin)

# Plotting data
bin_number = np.linspace(1.5, 6.5, 150)  # approximately 0.03 AU bin width

fig, ax1 = plt.subplots(1, figsize=(10,6))
ax1.hist(a_list_begin, bins=bin_number, histtype='step', color='darkorange', label='start', linewidth=2.0)
ax1.hist(a_list_end, bins=bin_number, histtype='step', color='darkblue', label='10.000 yrs', linewidth=2.0)
ax1.set_xlabel("Semi-major axis (AU)", fontsize=20), ax1.set_ylabel("Count", fontsize=20)
ax1.tick_params(axis='x', labelsize=20), ax1.tick_params(axis='y', labelsize=20)
ax1.axvline(x=2.5, ls='--', color='black')
ax1.axvline(x=2.82, ls='--', color='black')
ax1.axvline(x=3.3, ls='--', color='black')
ax1.text(2.55, 680, '3:1', fontsize=18)
ax1.text(2.87, 680, '5:2', fontsize=18)
ax1.text(3.35, 680, '2:1', fontsize=18)
ax1.legend(prop={'size': 18})
ax1.set_xlim(1.5,6.2)
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(2)
plt.show()

fig, ax1 = plt.subplots(1, figsize=(10,10))
ax1.scatter(ast_x_end, ast_y_end, marker='.', s=2.)
ax1.scatter(jup_x_end,jup_y_end, color='brown', s=30.)
ax1.scatter(sun_x_end, sun_y_end, s=50., color='orange')
ax1.text(-3.5, -5, 'Jupiter', fontsize=20)
ax1.text(-0.5, -0.5, 'Sun', fontsize=20)
ax1.set_xlabel("x (AU)", fontsize=20), ax1.set_ylabel("y (AU)", fontsize=20)
ax1.tick_params(axis='x', labelsize=20), ax1.tick_params(axis='y', labelsize=20)
plt.show()

bin_size = int((max(trojans_list_end) - min(trojans_list_end)) / 10. )
fig, ax1 = plt.subplots(1, figsize=(8,6))
ax1.hist(trojans_list_end, bins=20, label='end', linewidth=2.0)
ax1.hist(trojans_list_begin, bins=20, histtype='step', color='blue', label='begin', linewidth=2.0)
ax1.set_xlabel("Angle (degree)", fontsize=20), ax1.set_ylabel("Count", fontsize=20)
ax1.tick_params(axis='x', labelsize=20), ax1.tick_params(axis='y', labelsize=20)
ax1.set_xlim(0,180)
ax1.axvline(x=60, ls='--', color='black')
font0 = FontProperties()
font = font0.copy()
font.set_weight('bold')
ax1.text(30, 61, 'Expected region',fontproperties=font, fontsize=16)
ax1.add_patch(patches.Rectangle((30, 0),60,65,alpha=0.3, color=(0,0.45,0.7)))
ax1.set_ylim(0,65)
plt.show()