# -*- coding: utf-8 -*-
"""
Created on Thu Feb 01 15:13:31 2018
@author: Javier
"""

from __future__ import division
from numba import cuda
import math
import math as m
import numpy as np
from timeit import default_timer as timer
import matplotlib.pyplot as plt
import pickle

np.random.seed(123456789)
#------------------------------  Integration functions and saving/loading  ------------------------------
@cuda.jit  #Solves the differential equation for the asteroids in the GPU
def fast(xast, xsun, xjup, vast, vsun, vjup, astforce):
    """Perform matrix multiplication of C = A * B
    """

    dt = .5/365
    row, col = cuda.grid(2)
    if row < astforce.shape[0] and col < astforce.shape[1]:
        astforce[row, col] = 0.0376291660908*(1047.945205*(xsun[0,col]-xast[row,col])/(m.sqrt((xsun[0,0]-xast[row,0])**2+(xsun[0,1]-xast[row,1])**2+(xsun[0,2]-xast[row,2])**2)**3 )+(xjup[0,col]-xast[row,col])/(m.sqrt((xjup[0,0]-xast[row,0])**2+(xjup[0,1]-xast[row,1])**2+(xjup[0,2]-xast[row,2])**2)**3 )) 
        vast[row,col] = vast[row,col] + astforce[row,col]*dt
        xast[row,col] = xast[row,col] + vast[row,col]*dt
    cuda.syncthreads()


@cuda.jit #Solves the differential equation for the Sun and Jupiter in the GPU
def fstel(xsun, vsun, xjup, vjup):
 
    dt = .5/365
    cuda.syncthreads()    
    col = cuda.grid(1)
    if col < xsun.shape[1]:
        dv = dt * 0.0376291660908 * (xjup[0,col]-xsun[0,col])/(m.sqrt((xjup[0,0]-xsun[0,0])**2+(xjup[0,1]-xsun[0,1])**2+(xjup[0,2]-xsun[0,2])**2)**3 )   
        vsun[col] += dv
        vjup[col] -= 1047.945205  * dv
        cuda.syncthreads()
        xsun[0,col] += vsun[col]*dt
        xjup[0,col] += vjup[col]*dt


def save_obj(obj, name ):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f)


def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        global store
        store =pickle.load(f)
        return store


#------------------------------  Constants/variables  ------------------------------
        
M_jup_scaling = 1.8982e+27 # normalization factor for mass
AU_scaling = 1.495978707e+11 # normalization factor for length
year_scaling = 365 * 24 * 3600 # normalization factor for time
G = 6.67408e-11 * AU_scaling**(-3) * M_jup_scaling * year_scaling**2
M_sun = 1.989e+30 / M_jup_scaling
e = 0.04839266
a = 5.2044
R_jup = ((1-e))*a
M_jup = 1.

V_jup = -np.sqrt((G*M_sun/ a)*((1+e)/(1-e)))
nbodies = 50000 #Numer of asteroids       
dt = .5/365 # in years
t_end = 100# in years
t_intervals = int(t_end / dt)


#------------------------------  Initial conditions  ------------------------------
xsun = np.array([0.,-M_jup/M_sun * R_jup,0.], 'f')
vsun = np.array([-(V_jup*M_jup)/M_sun,0,0], 'f') #x,y,z,vx, vy, vz
xjup = np.array([0,R_jup,0], 'f')
vjup = np.array([V_jup,0,0], 'f')

#enable this for eccentricity 
# =============================================================================
## =============================================================================
theta = np.random.uniform(0, high=2*np.pi, size=nbodies).astype('f')     #Phase of the orbits
r = np.random.uniform(2, high=6, size=nbodies).astype('f')
v = np.sqrt(G*M_sun/r) * np.random.uniform(0.99, high=1.01, size=nbodies).astype('f')
vz = v*np.random.uniform(0, 0.005, size=nbodies).astype('f')
z = np.zeros(nbodies)
xast = np.stack((r*np.cos(theta),r*np.sin(theta), z), axis=-1)
vast = np.stack((v*np.cos(theta+m.pi/2.),v*np.sin(theta+m.pi/2.),vz), axis=-1)
# =============================================================================
# =============================================================================
#enable this for no eccentricity
#theta = np.random.uniform(0, high=2*np.pi, size=nbodies).astype('f')
#r = np.random.uniform(2, high=5, size=nbodies).astype('f')
#v = np.sqrt(G*M_sun/r)
#vz = np.zeros(nbodies)
#z = np.zeros(nbodies)
#xast = np.stack((r*np.cos(theta),r*np.sin(theta), z), axis=-1)
#vast = np.stack((v*np.cos(theta+m.pi/2.),v*np.sin(theta+m.pi/2.),vz), axis=-1)
# =============================================================================
# =============================================================================
# Copy the arrays to the device
xast_global_mem = cuda.to_device(xast)
xsun_global_mem = cuda.to_device(np.array([xsun]))
xjup_global_mem = cuda.to_device(np.array([xjup]))
vast_global_mem = cuda.to_device(vast)
vsun_global_mem = cuda.to_device(vsun)
vjup_global_mem = cuda.to_device(vjup)
astforce_global_mem = cuda.device_array((nbodies, 3)) # Allocate memory on the GPU for the result

##------------------------------  Integration  ------------------------------
# Configure the blocks
threadsperblock = (16, 16)
blockspergrid_x = int(math.ceil(xast.shape[0] / threadsperblock[0]))
blockspergrid_y = int(math.ceil(np.array([xsun]).shape[1] / threadsperblock[1]))
blockspergrid = (blockspergrid_x, blockspergrid_y)

blockspergrid_x1 = int(math.ceil(xast.shape[0] / threadsperblock[0]))
blockspergrid_y1 = int(math.ceil(np.array([xsun]).shape[1] / threadsperblock[1]))
blockspergrid1 = (blockspergrid_x1, blockspergrid_y1)


start = timer()

print "Number of intervals: ", t_intervals
time = timer()


k = 0 #

for i in range(t_intervals):
    
    fast[blockspergrid, threadsperblock](xast_global_mem, xsun_global_mem, xjup_global_mem, vast_global_mem, vsun_global_mem, vjup_global_mem, astforce_global_mem)

    fstel[blockspergrid, threadsperblock](xsun_global_mem, vsun_global_mem, xjup_global_mem, vjup_global_mem)

    if (i%(t_intervals/1000)==0) and i !=0: 
                print str(round(i*100./t_intervals,2))+"%" + '  dt:  ' + str(round((timer() - time) /  i * (t_intervals - i),0)), xast.shape, vast.shape

    if i * dt > k * 5000: #Saving and getting rid of scaping asteroids every 5000 years
        k += 1
        xast = xast_global_mem.copy_to_host()           
        vast = vast_global_mem.copy_to_host() 
        
        xast_global_mem = cuda.to_device(xast[np.amax(np.absolute(xast[:]),axis=1)<2*R_jup])
        vast_global_mem = cuda.to_device(vast[np.amax(np.absolute(xast[:]),axis=1)<2*R_jup])
        astforce_global_mem = cuda.device_array((len(xast[np.amax(np.absolute(xast[:]),axis=1)<2*R_jup]), 3))
#        
        xast = xast_global_mem.copy_to_host()
        xsun = xsun_global_mem.copy_to_host() 
        xjup = xjup_global_mem.copy_to_host()
        vast = vast_global_mem.copy_to_host()
        vsun = vsun_global_mem.copy_to_host() 
        vjup = vjup_global_mem.copy_to_host()
        
        save_obj(xsun, "xsun" + '_' + str(k))
        save_obj(xjup, "xjup" + '_' + str(k))
        save_obj(xast, "xast" + '_' + str(k))
        save_obj(vsun, "vsun" + '_' + str(k))
        save_obj(vjup, "vjup" + '_' + str(k))
        save_obj(vast, "vast" + '_' + str(k))

duration = timer() - start 
print 'actual simulation time: ' + str(duration)

#
#
##------------------------------  Saving/loading  ------------------------------
##Getting the final data from the GPU
xast = xast_global_mem.copy_to_host()
xsun = xsun_global_mem.copy_to_host() 
xjup = xjup_global_mem.copy_to_host()
vast = vast_global_mem.copy_to_host()
vsun = vsun_global_mem.copy_to_host() 
vjup = vjup_global_mem.copy_to_host()
#Saving the data to a file
save_obj(xsun, "xsun" + '_' + str(int(t_end/1000)))
save_obj(xjup, "xjup" + '_' + str(int(t_end/1000)))
save_obj(xast, "xast" + '_' + str(int(t_end/1000)))
save_obj(vsun, "vsun" + '_' + str(int(t_end/1000)))
save_obj(vjup, "vjup" + '_' + str(int(t_end/1000)))
save_obj(vast, "vast" + '_' + str(int(t_end/1000)))

## =============================================================================
#
##------------------------------  Plotting  ------------------------------
plt.figure(figsize=(12,12))

plt.plot(xast[:,0], xast[:,1], 'o', c = 'g', markersize = 0.1)  
plt.plot(xsun[0,0], xsun[0,1], 'o', c = 'y') 
plt.plot(xjup[0,0], xjup[0,1], 'o', c = 'r')
plt.xlim(-10,10)
plt.ylim(-10,10)
plt.show()
