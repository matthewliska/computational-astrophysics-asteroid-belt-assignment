from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rcParams['axes.linewidth'] = 2


a = 5.2044             # semi major axis (units of AU)
e = 0.0489          # eccentricity 
G = 4*(np.pi**2)    # gravitational constant (units of M_sun, AU and year)
M_sun = 1.          # solar mass 
M_jup = (1.0/1047)*M_sun  # fraction of M_sun


def simulation(time, h, number_of_astroids):
    '''This function uses the Euler-Cromer method to solve N times 3-body problems.'''
    
    a_pos = open("second_terminal_astroid_positions_time_7feb_m=1_abound=3.3_3.4_" +str(time)+"_h_"+str(h)+"_n_"+str(number_of_astroids)+".dat", "a")
    a_vel = open("second_terminal_astroid_velocities_time_7feb_m=1_abound=3.3_3.4_"+str(time)+"_h_"+str(h)+"_n_"+str(number_of_astroids)+".dat", "a")
    j_pos = open("second_terminal_jupiter_positions_time_7feb_m=1_abound=3.3_3.4_" +str(time)+"_h_"+str(h)+"_n_"+str(number_of_astroids)+".dat", "a")
    j_vel = open("second_terminal_jupiter_velocities_time_7feb_m=1_abound=3.3_3.4_"+str(time)+"_h_"+str(h)+"_n_"+str(number_of_astroids)+".dat", "a")
    d_astroid = open("second_terminal_distance_astroids_time_7feb_m=1_abound=3.3_3.4_" +str(time)+"_h_"+str(h)+"_n_"+str(number_of_astroids)+".dat", "a")

    '''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''
    '''%%% creation and initialization of jupiter and astroids '''
    '''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''
    
    '''jupiter, take sun = (0,0,0) '''
    jupiter = np.array([[ [0.0,a*(1.-e),0.0] , [-np.sqrt((G/a) * (1.+e)/(1-e)),0.0,0.0] ]])  
    
    '''create multidimensional array for all astroids'''
    astroids = np.zeros((number_of_astroids, 2, 3)) # nr astr., pos. and vel. vector, spatial dimension
    
    '''array for radius and angle for starting position'''
    radius0 = np.random.uniform(2.4, 3.4, (number_of_astroids))
    theta0  = np.random.uniform(0.0, 2*np.pi, (number_of_astroids))
    
    '''x = r cos theta'''    
    astroids[:,0][:,0] = radius0*np.cos(theta0)
    
    '''y = r sin theta'''
    astroids[:,0][:,1] = radius0*np.sin(theta0)
    
    '''z = small'''
    astroids[:,0][:,2] = 1/100*radius0*np.sin(theta0)
    
    '''vx and vy are random '''
    astroids[:,1][:,0] = np.random.uniform(-2, 2, (number_of_astroids))
#-np.sqrt(G/radius0 *(1.+e)/(1-e)) * np.sin(theta0)
    astroids[:,1][:,1] =  np.random.uniform(-2, 2, (number_of_astroids))
    
    '''vz = small'''
    astroids[:,1][:,2] = 0.01 * np.random.uniform(-2, 2, (number_of_astroids))
    
    
    '''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''
    '''%%% time for loop '''
    '''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''
    
    n = int(time/h) #number of timesteps
    for t in range(n-1):
        
        '''distance sun to jupiter'''
        d_sun_jup = np.sqrt(  jupiter[:,0][:,0]**2
                            + jupiter[:,0][:,1]**2
                            + jupiter[:,0][:,2]**2
                            )[:,None]
        
        '''distance sun to astroids'''
        d_sun_ast = np.sqrt(  astroids[:,0][:,0]**2
                            + astroids[:,0][:,1]**2
                            + astroids[:,0][:,2]**2
                            )[:,None]
        '''distance jupiter to astroids'''
        d_jup_ast = np.sqrt(  (astroids[:,0][:,0]-jupiter[:,0][:,0])**2
                            + (astroids[:,0][:,1]-jupiter[:,0][:,1])**2
                            + (astroids[:,0][:,2]-jupiter[:,0][:,2])**2
                            )[:,None]
       
        '''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''
        '''%%% Euler-Cromer method to calculate velocity and position'''
        '''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''
        
        ''' calculate the evolution jupiter through time'''
        jupiter[:,1] += h *    (-G/(d_sun_jup**3)* jupiter[:,0])
        jupiter[:,0] += h * jupiter[:,1]

        ''' calculate the evolution astroids through time'''
        astroids[:,1] += h * (-G/(d_sun_ast**3)* astroids[:,0] 
                           + (-G*M_jup/(d_jup_ast**3) * (astroids[:,0] - jupiter[:,0]) ) )
        astroids[:,0] += h * astroids[:,1]   
        
        
        '''delete diverging astroids'''
        bound_astroids = np.logical_and( d_sun_ast[:,0] > 1.7,  d_sun_ast[:,0] < 4.0 )
        astroids = astroids[bound_astroids]
        
        '''saves data every 100000 timesteps.'''
        if t %  100000 == 0:
            np.savetxt(a_pos,astroids[:,0])
            np.savetxt(a_vel,astroids[:,1])
            np.savetxt(j_pos,jupiter[:,0])
            np.savetxt(j_vel,jupiter[:,1])
            np.savetxt(d_astroid, d_sun_ast)    

  
    return jupiter, astroids 

'''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''
'''%%% running the simulation '''
'''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'''

total_years = 200000
timestep = 0.5/365.25
number_of_astroids = 10000
#jupiter, astroids = simulation(total_years, timestep, number_of_astroids)


#part_of_filename = '_time_7feb_20000_h_0.00136892539357_n_10000'


#j_pos = np.loadtxt('jupiter_positions'+str(part_of_filename)+'.dat')
#as_pos = np.loadtxt('astroid_positions'+str(part_of_filename)+'.dat')
#distance_astroid = np.loadtxt( 'distance_astroids_7feb_time_m=1_20000_h_0.00136892539357_n_10000.dat')



def convergence_test(time, h):
    '''This function is to investigate the error convergence of the Euler-Cromer method for a 3 planet system. However, it does not work correctly.'''

    planet_one_pos = open("planet_one_positions_time_" +str(time)+"_h_"+str(h)+".dat", "a")
    planet_one_vel = open("planet_one_velocities_time_"+str(time)+"_h_"+str(h)+".dat", "a")
    planet_two_pos = open("planet_two_positions_time_" +str(time)+"_h_"+str(h)+".dat", "a")
    planet_two_vel = open("planet_two_velocities_time_"+str(time)+"_h_"+str(h)+".dat", "a")
    planet_three_pos = open("planet_three_positions_time_" +str(time)+"_h_"+str(h)+".dat", "a")
    planet_three_vel = open("planet_three_velocities_time_"+str(time)+"_h_"+str(h)+".dat", "a")
    distance_planets = open("distance_planets_time_" +str(time)+"_h_"+str(h)+".dat", "a") 
    fig, axes = plt.subplots(nrows=1, ncols=1)
    w, h = fig.get_size_inches()
    fig.set_size_inches(1.2 * w, 2.4* h)
    
    
    planet_one   = np.array([[ [np.cos(  np.pi/3),np.sin(  np.pi/3),0.0] , [-np.sin(  np.pi/3),np.cos(  np.pi/3),0.0] ]])  
    planet_two   = np.array([[ [np.cos(2*np.pi/3),np.sin(2*np.pi/3),0.0] , [-np.sin(2*np.pi/3),np.cos(2*np.pi/3),0.0] ]])  
    planet_three = np.array([[ [np.cos(  np.pi  ),np.sin(  np.pi  ),0.0] , [-np.sin(  np.pi  ),np.cos(  np.pi  ),0.0] ]])  

    n = int(time/h) #number of timesteps
    for t in range(n-1):
        '''calculate the distances between the three planets'''

        d_one_two = np.sqrt(    (planet_one[:,0][:,0]-planet_two[:,0][:,0])**2
                              + (planet_one[:,0][:,1]-planet_two[:,0][:,1])**2
                              + (planet_one[:,0][:,2]-planet_two[:,0][:,2])**2
                              )[:,None]
        
        d_one_three = np.sqrt(  (planet_one[:,0][:,0]-planet_three[:,0][:,0])**2
                              + (planet_one[:,0][:,1]-planet_three[:,0][:,1])**2
                              + (planet_one[:,0][:,2]-planet_three[:,0][:,2])**2
                              )[:,None]
                
        d_two_three = np.sqrt(  (planet_two[:,0][:,0]-planet_three[:,0][:,0])**2
                              + (planet_two[:,0][:,1]-planet_three[:,0][:,1])**2
                              + (planet_two[:,0][:,2]-planet_three[:,0][:,2])**2
                              )[:,None]
                        
        ''' calculate the evolution of the planets through time'''

        planet_one[:,1]   += h * (-2*G/(d_one_two**3) * (planet_one[:,0]-planet_two[:,0])) 
        + h * (-2*G/(d_one_three**3) * (planet_one[:,0]-planet_three[:,0]))
        planet_one[:,0]   += h * planet_one[:,1]
            
        planet_two[:,1]   += h * (-2*G/(d_one_two**3) * (planet_two[:,0]-planet_one[:,0]))
        + h * (-2*G/(d_two_three**3) * (planet_two[:,0]-planet_three[:,0]))
        planet_two[:,0]   += h * planet_two[:,1]
            
        planet_three[:,1] += h * (-2*G/(d_one_three**3) * (planet_three[:,0] - planet_one[:,0]))
        + h * (-2*G/(d_two_three**3) * (planet_three[:,0] - planet_two[:,0]))
        planet_three[:,0] += h * planet_three[:,1]
        
        '''saves data every 100000 timesteps.'''
        if t < 1000000:
            np.savetxt(planet_one_pos,planet_one[:,0])
            np.savetxt(planet_two_pos,planet_two[:,0])
            np.savetxt(planet_three_pos,planet_three[:,0])
            np.savetxt(planet_one_vel,planet_one[:,1])
            np.savetxt(planet_two_vel,planet_two[:,1])
            np.savetxt(planet_three_vel,planet_three[:,1])
            np.savetxt(distance_planets, distance_planets)    

    return planet_one, planet_two, planet_three, distance_planets

#convergence_test(10,0.01)


def plot_planets(planet_one, planet_two, planet_three, nbins=250, zoom=6):
    '''This function is an aid to plot the planets investigated in the convergence_test function.'''
    fig, axes = plt.subplots(nrows=1, ncols=1)
    w, h = fig.get_size_inches()
    fig.set_size_inches(1.2 * w, 2.4* h)

    axes.scatter((planet_one[:,0]), (planet_one[:,1]), s=20, label="planet one", c=c1)
    axes.scatter((planet_two[:,0]), (planet_two[:,1]), s=20, label="planet two", c=c2)
    axes.scatter((planet_three[:,0]), (planet_three[:,1]), s=20, label="planet three", c=c3)
    axes.scatter(0.0 , 0.0, s= 50,c='y')

    axes.set_xlabel("x$_{position}$ [AU]", fontsize=20)
    axes.set_ylabel("y$_{position}$ [AU]", fontsize=20)
    axes.set_title("Trajectory of Jupiter and Asteriods",fontsize=20)
    axes.tick_params (axis='both',labelsize=20, length=20,width=2)
    axes.legend (prop={'size': 25},markerscale=1,fancybox=True,loc=2)
    axes.set_xlim(-zoom, zoom)
    axes.set_ylim(-zoom, zoom)   
    plt.show()

#plot_planets(planet_one, planet_two, planet_three)
    
    

def plot_data(astroid_position, jupiter_position, distance, nbins=250, zoom=6):
    '''This function is an aid to plot the astroids, Jupiter and the stationary Sun as calulated by the simulation function.'''

    fig, (axes,axes2)= plt.subplots(nrows=2, ncols=1)
    w, h = fig.get_size_inches()
    fig.set_size_inches(2.2 * w, 5.* h)

    axes.scatter(0.0 , 0.0, s= 80, label="Sun",c=c2)
    axes.scatter((jupiter_position[:,0]), (jupiter_position[:,1] ), s=20, label="Jupiters orbit",c=c3)
    axes.scatter((astroid_position[:,0]), (astroid_position[:,1]), s=5, label="bound Astroids", c=c1)
    axes2.hist(distance, bins=nbins,  color=c1)
    axes2.set_xlabel("radial distance to sun (AU)", fontsize=20)
    axes2.set_ylabel("number of bound asteroids", fontsize=20)
    
    axes2.axvline(x=2.5,  color=c2, linestyle='dotted', linewidth=6)
    axes2.axvline(x=2.8,  color=c2, linestyle='dotted', linewidth=6)
    axes2.axvline(x=2.951,  color=c2, linestyle='dotted', linewidth=6)
    axes2.axvline(x=3.375,  color=c2, linestyle='dotted', linewidth=6)
    axes2.text(2.51  ,12.0,'3:1', fontsize=15)
    axes2.text(2.81  ,12.0,'5:2', fontsize=15)
    axes2.text(2.961 ,12.0,'7:3', fontsize=15)
    axes2.text(3.385 ,12.0,'2:1', fontsize=15)

    axes.set_xlabel("x$_{position}$ [AU]", fontsize=20)
    axes.set_ylabel("y$_{position}$ [AU]", fontsize=20)
    axes.set_title("Trajectory of Jupiter and Asteriods",fontsize=20)
    axes.tick_params (axis='both',labelsize=20, length=20,width=2)
    axes2.tick_params(axis='both',labelsize=20, length=20,width=2)
    axes.legend (prop={'size': 15},markerscale=1,fancybox=True,loc=2)
    axes.set_xlim(-zoom, zoom)
    axes.set_ylim(-zoom, zoom)
    axes.axis('equal')
    plt.tight_layout()

    plt.show()
    
c1= '#66c2a5'
c2= '#fc8d62'
c3= '#8da0cb'

#plot_data(a_pos[-400:], j_pos, distance[-400:], nbins=25, zoom=6)


